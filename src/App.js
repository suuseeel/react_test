import React from 'react';
import { Route, BrowserRouter, Switch } from 'react-router-dom';
import Page1 from './pages/Page1';
import Page3 from './pages/Page3';
import Page2 from './pages/Page2';
import LoginPage from './pages/LoginPage';
import NavBar from './components/NavBar';

import './App.css';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
            <Route path='/table' exact component={Page1} />  
            <Route path='/unsplash' exact component={Page3} />  
            <Route path='/todo' exact component={Page2} />  
            <Route path='/' exact component={LoginPage} />  
        </Switch>
        </BrowserRouter>
    </div>
  );
}

export default App;
