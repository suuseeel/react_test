import React, { useEffect, useState } from 'react'

const EditModal = ({ post,savePost,editing,length }) => {
    const [title, setTitle] = useState('')
    const [body, setBody] = useState('');
    const [userId, setUserId] = useState();
    const submitPost = () => {
        if (!post) {
            let npost = {
                title: title,
                body: body,
                userId:userId,
                id:new Date().valueOf()      
            }
            savePost(npost) 
            setTitle('')
            setBody('')
            setUserId('')  
        } else {
            let newPost = {
                ...post,
                title: title,
                body: body,
                userId:userId
            }
            savePost(newPost)  
            setTitle('')
            setBody('')
            setUserId('')
        } 
    }
    const clrFild = () => {
            setTitle('')
            setBody('')
            setUserId('')
    }
    useEffect(() => {
        if (post) {
            setTitle(post.title)
            setBody(post.body)
            setUserId(post.userId)
        }
    },[post])
    return (
        <div>
            <div className="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="exampleModalLabel">{ editing?'Update Post':'Add Post'}</h5>
                        <button type="button" onClick={clrFild} className="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <div className="form-group">
                                <label for="exampleInputEmail1">Title</label>
                                <input  name="title" className="form-control" value={title}  onChange={(e)=>setTitle(e.target.value)} />
                        </div>
                            <div className="form-group">
                                <label for="exampleInputPassword1">Description</label>
                                <input name="body"  className="form-control" value={body} onChange={(e) => setBody(e.target.value)} />
                            </div>
                            <div className="form-group">
                                <label for="exampleInputPassword1">UserId</label>
                                <input name="body" className="form-control" value={userId} onChange={(e) => setUserId(e.target.value)}/>
                                
                            </div>
  
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" className="btn btn-primary"  data-dismiss="modal" onClick={submitPost}>submit</button>
                    </div>
                    </div>
                </div>
                </div>
        </div>
    )
}

export default EditModal;
