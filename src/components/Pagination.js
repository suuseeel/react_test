import React ,{useState,useEffect} from 'react'

function Pagination({ posts, pageLimit, dataLimit,userId,setPostList }) {
    const [pages,setPage] = useState(Math.round(posts.length / dataLimit));
    const [currentPage, setCurrentPage] = useState(1);

    useEffect(() => {
        getPaginatedData()
        let pages = Math.round(posts.length / dataLimit);
        setPage(pages)
    }, [posts,dataLimit,pageLimit,currentPage])
    
    function goToNextPage() {   
        setCurrentPage((page) => page + 1);
        getPaginatedData()
    }
  
    function goToPreviousPage() {
        setCurrentPage((page) => page - 1);
        getPaginatedData()
    }
  
    function changePage(event) {
        const pageNumber = Number(event.target.textContent);
        setCurrentPage(pageNumber);
    }
  
    const getPaginatedData = () => {
        const startIndex = currentPage * dataLimit - dataLimit;
      const endIndex = startIndex + dataLimit;
      console.log(startIndex, endIndex);
      console.log(posts.slice(startIndex, endIndex))
        setPostList(posts.slice(startIndex, endIndex))
    }
  
    const getPaginationGroup = () => {
        let start = Math.floor((currentPage - 1) / pageLimit) * pageLimit;
        return new Array(pageLimit).fill().map((_, idx) => start + idx + 1);
    };
    return (
        <div>
        <div className="pagination">
          <button
            onClick={goToPreviousPage}
            className={`prev ${currentPage === 1 ? 'disabled' : ''}`}
          >
            prev
          </button>

          {getPaginationGroup().map((item, index) => (
            <button
              key={index}
              onClick={changePage}
              className={`paginationItem ${currentPage === item ? 'active' : null}`}
            >
              <span>{item}</span>
            </button>
          ))}
    
          <button
            onClick={goToNextPage}
            className={`next ${currentPage === pages ? 'disabled' : ''}`}
          >
            next
          </button>
        </div>
      </div>
    );
}
  

export default Pagination;