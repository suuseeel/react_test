import React,{useEffect,useState} from 'react'
import EditModal from './EditModal';
import Pagination from './Pagination';

const PostTable = ({ posts, userId }) => {
    const [postList, setPostList] = useState([])
    const [listShow, setListShow] = useState([])
    const [editing, setEditing] = useState(false);
    const [posToEdit, setPost] = useState(null);
    
    useEffect(() => {
        if (userId !== null) {
            let filterPosts = posts && posts.length > 0 && posts.filter((data) => data.userId == userId);
            console.log(filterPosts)
            setListShow(filterPosts)
         } else {
             if (posts && posts.length > 0) {
              setPostList(posts)
            }
            
        }
    }, [userId, posts,postList,editing])
    
    const editPost = (id) => {
        
        setEditing(true);
        let post = listShow.filter((post) => post.id === id)[0]
        // console.log(post)
        setPost(post) 
        // console.log(posToEdit)
    }
   
    const savePost = (data) => {
        const post = postList.findIndex((post) => post.id == data.id);
        if (post >= 0) {
            let newPost = listShow[post]
        newPost.title = data.title;
        newPost.body = data.body;
        let newList=[...listShow]
        setListShow(newList)
        setEditing(false);
        } else {
            let newList = listShow;
            newList.push(data) 
            setListShow(newList)
            setEditing(false);
        }    
    }

    const addPost = () => {
        setPost(null)
        setEditing(true)
    }
    const deletePost = (id) => {
        alert('are you sure?')
        let posts = listShow.filter((data) => data.id !== id)
        setListShow(posts)
    }

    return (
        <div className='position-relative'>
            <div className='float-right addew'>
            <button className='btn btn-info' data-toggle="modal" data-target="#exampleModal" onClick={addPost}>Add new Post</button>
            
                {editing ? 
                    <EditModal editing={editing} post={posToEdit} savePost={savePost} length={postList.length}/>
                    : null
             } 
            </div>
            
            <div className='container pt-5'>
                <div className='table-responsive'>
                <table className="table table-striped">
                <thead>
                    <tr>
                        <th >S.No.</th>
                        <th>Title</th>
                        <th>Body</th>
                        <th>UserId</th>
                        <th >Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {listShow && listShow.length > 0?listShow.map((post, indx) =>
                         <tr key={indx}>
                            <td>{indx+1}</td>
                            <td>{post.title}</td>
                            <td>{post.body}</td>
                            <td>{post.userId}</td>
                            <td><button className='btn btn-info mr-2' data-toggle="modal" data-target="#exampleModal" onClick={()=>editPost(post.id)}>edit</button>
                            <button className='btn btn-danger' onClick={()=>deletePost(post.id)}>delete</button>
                            </td>
                            </tr>
                    ):null}
                       
                    </tbody>
            </table>
                </div>
                
          </div>
         
            <Pagination posts={postList} pageLimit={5}
                dataLimit={10}
                setPostList={setListShow} />
        </div>
    )
}

export default PostTable
