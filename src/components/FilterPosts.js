import React, { useState } from 'react'

const FilterPosts = ({ posts ,userId,setUserId,handlePost}) => {
    const [userIds, setUserIds] = useState([1,2,3,4,5,6,7,8,9,10])

    const handleChange = (e) => {
        setUserId(e.target.value);
        // handlePost(e.target.value)
    }
    

    return (
        <div className='col-4'> 
            <label className='text-left w-100' >Filer Post By UserId:</label>
            <select value={userId?userId:''} onChange={handleChange} className='form-control'>
                <option>select userId</option>
                {posts && posts.length > 0 ? userIds.map((id,indx) =>
                    <option key={indx} value={id}>{id}</option>
                ):null}
               
              
            </select>
        </div>
    )
}

export default FilterPosts
