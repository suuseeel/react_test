import React, { useState } from 'react'

const Todo = () => {
    const [task,setTask]=useState('')
    const [editing,setEditing]=useState(false)
    const [indx,setindx]=useState(null)
    const [taskList, setTaskList] = useState([]);

    const addTaskToList = (event) => {
        event.preventDefault();
        if (task !== '') {
            if (!editing) {
                let tasklist = [...taskList,task]
                setTaskList(tasklist);
                setTask('')
    
            } else {
                let tasklist=taskList
                tasklist[indx] = task;
                setTaskList(tasklist);
                setTask('');
                setEditing(false);
                setindx(null)
            }
           
        }else{
            alert('task cannot be empty')
        }
       
    }
    const editTask = (indx) => {
        setEditing(true)
        setTask(taskList[indx])
        setindx(indx)
    }
    const deleteTask = (indx) => {
        let tasklist = taskList.filter((task, id) => id !== indx);
        setTaskList(tasklist);
    }

    return (
        <div>
            <form className="form-inline" >
                <input name="task" placeholder='task' value={task} onChange={(e) => setTask(e.target.value)} className='form-control mb-2 mr-sm-2'/>
                <button className='btn btn-primary' onClick={addTaskToList} data-toggle="modal" data-target="#exampleModal">{editing?'Update task':'Add task'}</button>
            </form>
                
          
            <div className='table-responsive mt-4'>
            
            <table className='table table-bordered'>
            <thead>
                <tr>
                    <th >S.No.</th>
                     <th>task</th>
                    <th >Actions</th>
                </tr>
                </thead>
                <tbody>
                {
                   taskList&&taskList.length > 0 ? taskList.map((task,indx)=> 
                   <tr key={indx}>
                      <td>{indx+1}</td>
                           <td>{task}</td>
                           <td >
                               <button className='btn btn-primary mr-3' onClick={() => editTask(indx)}>edit</button>
                               <button  className='btn btn-danger' onClick={() => deleteTask(indx)}>Delete</button>
                           </td>
                  </tr>    
                  ):null
                        }
             </tbody> 
             </table>
            </div>
        </div>
    )
}

export default Todo
