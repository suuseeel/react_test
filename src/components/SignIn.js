import React, { useState } from 'react'
import { useDispatch } from "react-redux";
import { useHistory } from 'react-router-dom/cjs/react-router-dom.min';
import { loginAction } from '../action/userActions';

const SignIn = () => {
    const history=useHistory()
     const [email,setEmail]=useState('')
     const [password,setPassword]=useState('')
    const dispatch = useDispatch();

  const submitUser = async (e) => {
    e.preventDefault();
        const data = {
            email: email,
            password:password
        }
        const response =await dispatch(loginAction(data));
        if (response.status) {
          history.push('/table')
        } else {
            alert(response.message)
      }

    }
    return (
      <div>
         <div id="login">
        <div className="container">
            <div id="login-row" className="row justify-content-center align-items-center">
                <div id="login-column" className="col-md-6">
                    <div id="login-box" className="col-md-12">
                        <form id="login-form" className="form" action="" method="post">
                            <h3 className="text-center text-info">Login</h3>
                            <div className="form-group">
                                <label for="username" className="text-info">Email:</label><br/>
                                <input type="text" name="username" placeholder='email' id="username"onChange={(e)=>setEmail(e.target.value)} className="form-control" />
                            </div>
                            <div className="form-group">
                                <label for="password" className="text-info">Password:</label><br/>
                                <input type="text" name="password" placeholder='password' id="password" onChange={(e)=>setPassword(e.target.value)} className="form-control" />
                            </div>
                            <div className="form-group">
                                <input type="submit" name="submit" className="btn btn-info btn-md" onClick={submitUser} value="submit" />
                            </div>
                           
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
        {/* <div className="container">
            <label for="uname"><b>Username</b></label>
                 <input type="text" placeholder='email' onChange={(e)=>setEmail(e.target.value)} required />
            <label for="psw"><b>Password</b></label>
                 <input type="password" placeholder="Enter Password"  onChange={(e)=>setPassword(e.target.value)} name="psw" required />
            <button onClick={submitUser}>Login</button>
        </div>   */}
      </div>
  )
  
}



export default SignIn;
