import React from 'react'
import { Link } from 'react-router-dom';
const sideMenu = () => {
    return (
        <div id="sidebar-wrapper">
                <ul className="sidebar-nav">
                    <li className="sidebar-brand"> <Link href="#"> Start Bootstrap </Link> </li>
                    <li><Link to="/">Login Page</Link>  </li>
                    <li><Link to="/unsplash">Unsplash Page</Link>  </li>
                    <li><Link to="/table">Posts Table</Link>  </li>
                    <li><Link to="/todo">Todo</Link>  </li>
                </ul>
            </div>
        
    )
}

export default sideMenu;