import React from 'react'
import { Link } from 'react-router-dom';
const NavBar = () => {
    return (
        <nav className="navbar navbar-expand navbar-dark bg-primary"> <a href="#menu-toggle" id="menu-toggle" className="navbar-brand"><span className="navbar-toggler-icon"></span></a> <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample02" aria-controls="navbarsExample02" aria-expanded="false" aria-label="Toggle navigation"> <span className="navbar-toggler-icon"></span> </button>
            <div className="collapse navbar-collapse" id="navbarsExample02">
                <ul className="navbar-nav mr-auto">
                        <li><Link to="/">Login Page</Link>  </li>
                        <li><Link to="/unsplash">Unsplash Page</Link>  </li>
                        <li><Link to="/table">Posts Table</Link>  </li>
                        <li><Link to="/todo">Todo</Link>  </li>
                </ul>
                <form className="form-inline my-2 my-md-0"> </form>
            </div>
        </nav>
        
    )
}

export default NavBar;
