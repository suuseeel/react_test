import axios from 'axios';

let httpClient = axios.create({
    timeout:40000,
    headers: {
        "Content-Type": "application/json",
    }
});


httpClient.interceptors.request.use(function (config) {
    Object.assign(config.headers,{'Access-Control-Allow-Origin':'*'})
    // Object.assign(config.headers,{'source':getItemFromCookie('source')? getItemFromCookie('source'):null})
    return config;
  }, function (error) {
    return Promise.reject(error);
  });



httpClient.interceptors.response.use((response) => {
    return response
}, async function(error) {
    const originalRequest = error.config;
    if ((error.response.status === 401 || error.response.status === 403) && !originalRequest._retry) {
      originalRequest._retry = true;
     return httpClient(originalRequest);
    }
    return Promise.reject(error);
});



export default httpClient;