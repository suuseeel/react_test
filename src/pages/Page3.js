import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { getImagesAction } from "../action/unsplash";
import NavBar from "../components/NavBar";
import SideMenu from '../components/sideMenu'

const Page3 = () => {
    const [term, setTerm] = useState('')
    const dispatch = useDispatch();

   const imgList=useSelector((state)=>state.imagesList)
    const { loading, images } = imgList;
    const handleChange = (e) => {
    const {  value } = e.target;
    console.log(value)
    dispatch(getImagesAction(value))
    setTerm(value)
    }
    return (
        <>
            <NavBar />
            <div id="wrapper" className="toggled">
            
            <SideMenu/>
            
            <div id="page-content-wrapper">
                    <div className="container-fluid">
                    <div className="row justify-content-center">
                        <div className="col-12 col-md-10 col-lg-8">
                            <form className="card card-sm">
                                <div className="card-body row no-gutters align-items-center">
                                    <div className="col-auto">
                                        <i className="fas fa-search h4 text-body"></i>
                                    </div>
                        
                                    <div className="col">
                                        <input name="term" className="form-control form-control-lg form-control-borderless" onChange={handleChange} value={term} type="search" placeholder="Search topics or keywords" />
                                    </div>
                                </div>
                            </form>
                        </div>
                        
                    </div>
                    <div className="container pt-5">
                        <div className="row">
                        {!loading && images && images.length > 0 ? images.map((data) => <div className="card col-lg-3 col-sm-3 col-12  pl-0 pr-0 mr-2">
                                <img className="card-img-top" src={data.urls.regular} alt="" />
                                <div className="card-body">
                                <p className="card-text">{data.alt_description }</p>
                                </div>
                            </div>) : null
                                 }
                        </div>
                    </div>
                    
                   
                </div>
            
                </div>
            </div>
        </>
    )
    
}


export default Page3