import React from 'react'
import Todo from '../components/Todo';
import NavBar from '../components/NavBar';
import SideMenu from '../components/sideMenu';

const Page2 = () => {
    return (
        <>
            <NavBar />
            <div id="wrapper" className="toggled">
                <SideMenu />
                <div id="page-content-wrapper">
                    <div className="container-fluid">
                        <Todo />
                    </div>
                </div>
                
            </div>
            
            
        </>
    )
}

export default Page2;
