import React, { useState, useEffect } from 'react';
import PostTable from '../components/PostTable';
import FilterPosts from '../components/FilterPosts';
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { postsAction } from '../action/jsonplaceholder';
import Pagination from '../components/Pagination';
import NavBar from '../components/NavBar';
import SideMenu from '../components/sideMenu';
import { Link } from 'react-router-dom';

const Page1 = () => {
    const [userId, setUserId] = useState(null);
    const dispatch = useDispatch();
    const postArray = useSelector((state) => state.postsList);
    const { loading, posts } = postArray;
    useEffect(() => {
        dispatch(postsAction());
    }, [dispatch])
   
    return (
        <>
            <NavBar />
            <div id="wrapper" className="toggled">
            <SideMenu/>
            
            <div id="page-content-wrapper">
                <div className="container-fluid ">
                    <FilterPosts posts={posts} userId={userId} setUserId={setUserId}/>
                    <PostTable posts={posts} userId={userId} />        
                </div>
            
                </div>
            </div>
        </>
    )
}

export default Page1;