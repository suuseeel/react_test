import { combineReducers } from 'redux';
import { createStore, applyMiddleware } from 'redux'; 
import { unSplashReducer } from './unsplash';
import { postsReducer } from './postsreducer';
import thunk from 'redux-thunk';

const reducers = combineReducers({
    imagesList: unSplashReducer,
    postsList:postsReducer
});

// const initialState={
//     login:{userInfo:userInfoLS},
//     register:{userInfo:userInfoLS}
// }
export const store=createStore(reducers,applyMiddleware(thunk))