


export const postsReducer = (state=[],action) => {
    switch(action.type){
                case 'POSTS_REQUEST':
                    return {loading :true}
                case 'POSTS_REQUEST_SUCCESS':
                    return {loading:false,posts:action.payload}
                case 'POSTS_REQUEST_FAILED':
                    return {loading:false,error:action.payload}
                default :
                    return state
        
            }
}