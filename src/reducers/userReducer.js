


export const userReducer = (state={},action) => {
    switch(action.type){
                case 'LOGIN_REQUEST':
                    return {loading :true}
                case 'LOGIN_SUCCESS':  
                    return {loading:false,user:action.payload}
                case 'LOGIN_FAILED':
                    return {loading:false,error:action.payload}
                default :
                    return state
        
            }
}