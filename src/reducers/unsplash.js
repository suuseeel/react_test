


export const unSplashReducer = (state=[],action) => {
    switch(action.type){
                case 'IMAGES_REQUEST':
                    return {loading :true}
                case 'IMAGES_SUCCESS':
                    
                    return {loading:false,images:action.payload}
                case 'IMAGE_REQUEST_FAILED':
                    return {loading:false,error:action.payload}
                default :
                    return state
        
            }
}