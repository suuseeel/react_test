import httpClient from "../helper/http-client";


const imagesService = async(term) => {
    const response = await httpClient.get('https://api.unsplash.com/search/photos?query=' + term, {
        headers: {
            Authorization : 'Client-ID arGiP8pDIdBvq3Tqv50OgvU_9AdWN31piX3sx6QB4m4'
        }
    })
    console.log(response);
    return response.data
}

export {
    imagesService
}