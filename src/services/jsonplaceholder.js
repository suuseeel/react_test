import httpClient from "../helper/http-client";


const postsService = async() => {
    const response = await httpClient.get('https://jsonplaceholder.typicode.com/posts')
    return response
}

export {
    postsService
}