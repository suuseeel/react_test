import { loginService } from "../services/userServices";

export const loginAction = (data) =>async(dispatch)=> {
    try {
        dispatch({ type:'LOGIN_REQUEST' })
        const resp = await loginService(data);
        dispatch({ type: 'LOGIN_SUCCESS', payload: resp.user })
        localStorage.setItem('userInfo', JSON.stringify(resp.user))
        return resp
    } catch (error) {
        dispatch({type:'LOGIN_FAILED',
        payload: error.response && error.response.data.message ? error.response.data.message:error.message })
    }
}