import { imagesService } from "../services/unsplash";

export const getImagesAction = (term) =>async(dispatch)=> {
    try {
        dispatch({ type:'IMAGES_REQUEST' })
        const resp = await imagesService(term);
        dispatch({ type:'IMAGES_SUCCESS',payload:resp.results})
        
    } catch (error) {
        dispatch({type:'IMAGE_REQUEST_FAILED',
        payload: error.response && error.response.data.message ? error.response.data.message:error.message })
    }
}