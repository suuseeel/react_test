import { postsService } from "../services/jsonplaceholder";



export const postsAction = () =>async(dispatch)=> {
    try {
        dispatch({ type:'POSTS_REQUEST' })
        const resp = await postsService();
        dispatch({ type:'POSTS_REQUEST_SUCCESS',payload:resp.data})
        
    } catch (error) {
        dispatch({type:'POSTS_REQUEST_FAILED',
        payload: error.response && error.response.data.message ? error.response.data.message:error.message })
    }
}